package fr.cnam.foad.nfa035.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;

/**
 * pattern DAO
 */

public class BadgeWalletDAO {

    private final Logger LOG = LogManager.getLogger(BadgeWalletDAO.class);;
    private File walletDatabase;

    public BadgeWalletDAO(String s) {
        this.walletDatabase=new File(s);
    }

    /**
     * ajout d'un Badge
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException {
        FileWriter fileWriter = new FileWriter(walletDatabase);
        ImageSerializer serializer = new ImageSerializerBase64Impl();
        fileWriter.write((String) serializer.serialize(image));
        fileWriter.close();

    }

    /**
     *???
     * @param fileBadgeStream
     * @throws IOException
     */
    public void getBadge(OutputStream fileBadgeStream) throws IOException {
        ImageStreamingDeserializer deserializer= new ImageDeserializerBase64StreamingImpl(fileBadgeStream);
        deserializer.deserialize(walletDatabase);
    }
}
